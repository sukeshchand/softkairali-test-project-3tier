﻿CREATE TABLE [dbo].[Customer] (
    [CustomerId]    INT          NOT NULL,
    [CustomerCode]  VARCHAR (10) NOT NULL,
    [CustomerName]  VARCHAR (50) NOT NULL,
    [CreatedDate]   DATETIME     CONSTRAINT [DF_Customer_CreatedDate] DEFAULT (getutcdate()) NOT NULL,
    [CreatedUserId] SMALLINT     NOT NULL,
    [StatusId]      SMALLINT     NOT NULL,
    CONSTRAINT [PK_Customer_CustomerId (ASC)] PRIMARY KEY CLUSTERED ([CustomerId] ASC),
    CONSTRAINT [FK_Customer_Status_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[Status] ([StatusId]),
    CONSTRAINT [FK_Customer_User_UserID] FOREIGN KEY ([CreatedUserId]) REFERENCES [dbo].[User] ([UserId]),
    CONSTRAINT [UK_Customer_CustomerCode (ASC)] UNIQUE NONCLUSTERED ([CustomerCode] ASC)
);

