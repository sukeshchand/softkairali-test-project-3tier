﻿CREATE TABLE [dbo].[Status] (
    [StatusId]    SMALLINT       NOT NULL,
    [StatusCode]  VARCHAR (10)   NOT NULL,
    [StatusDescr] NVARCHAR (500) NOT NULL,
    CONSTRAINT [PK_Status_StatusId (ASC)] PRIMARY KEY CLUSTERED ([StatusId] ASC),
    CONSTRAINT [UK_Status_StatusCode (ASC)] UNIQUE NONCLUSTERED ([StatusCode] ASC)
);

