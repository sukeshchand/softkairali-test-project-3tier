﻿CREATE TABLE [dbo].[User] (
    [UserId]   SMALLINT       NOT NULL,
    [UserName] VARCHAR (50)   NOT NULL,
    [Password] NVARCHAR (500) NOT NULL,
    [StatusId] SMALLINT       NOT NULL,
    CONSTRAINT [PK_User_UserId (ASC)] PRIMARY KEY CLUSTERED ([UserId] ASC),
    CONSTRAINT [FK_User_Status_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[Status] ([StatusId]),
    CONSTRAINT [UK_User_UserName (ASC)] UNIQUE NONCLUSTERED ([UserName] ASC)
);

