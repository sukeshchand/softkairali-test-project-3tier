﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftKairali.Service.Interface
{
    public interface ICustomer
    {
        string GetCustomerName();
    }
}
