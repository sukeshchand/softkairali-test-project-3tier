﻿using System.Web;
using System.Web.Mvc;

namespace SoftKairali.WebMVCTestApp
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
