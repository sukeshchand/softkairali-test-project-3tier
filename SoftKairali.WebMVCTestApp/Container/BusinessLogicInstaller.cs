using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using SoftKairali.Service.Interface;
using SoftKairali.Service.Model;

namespace SoftKairali.WebMVCTestApp.Container
{
	public class BusinessLogicInstaller : IWindsorInstaller
	{
		public void Install(IWindsorContainer container, IConfigurationStore store)
		{
			container.Register(Component.For<ICustomer>().ImplementedBy<Customer>());
		}
	}
}