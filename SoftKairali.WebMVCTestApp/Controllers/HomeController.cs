﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SoftKairali.Service.Interface;

namespace SoftKairali.WebMVCTestApp.Controllers
{
    public class HomeController : Controller
    {
        private ICustomer _customer;
        public HomeController(ICustomer customer)
        {
            _customer = customer;
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}